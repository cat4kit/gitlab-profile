<!--SPDX-FileCopyrightText: 2023 Karlsruher Institut für TechnologieSPDX-License-Identifier: CC-BY-4.0-->
#### ![Cat4KIT](https://codebase.helmholtz.cloud/uploads/-/system/group/avatar/16928/Bildschirmfoto_2024-02-09_um_08.13.52.png?width=64): A cross-institutional data catalog framework for the FAIRification of environmental research data
____ 
[![CI](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/badges/main/pipeline.svg)](https://codebase.helmholtz.cloud/cat4kit/cat4kit-plumber/-/pipelines?page=1&scope=all&ref=main)
[![Docs](https://readthedocs.org/projects/cat4kit/badge/?version=latest)](https://cat4kit.readthedocs.io/en/latest/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)


The objective of the Cat4KIT project is to create a cross-institutional catalog and research data management system that will enhance the Findability, Accessibility, Interoperability, and Reusability (FAIR) principles of environmental research data. The framework comprises four distinct modules, each assigned with certain tasks.

 (1) Facilitating the availability of data on storage systems via clearly defined and standardized interfaces. 

 (2) The process of harvesting and creating (meta)data into uniform and standardized representations. 

 (3) Facilitating the public accessibility to (meta)data by the utilization of well defined and standardized catalog services and interfaces, so ensuring consistency and uniformity.

 (4) Allowing users to efficiently search, apply filters, and navigate through data obtained from decentralized research data infrastructures.
 


Every module is created and executed in an inter-institutional cooperation consisting of scientists, software developers, and possible end-users. This methodology guarantees the versatility of our framework, allowing it to be applied to many types of research data, including multi-dimensional climate model outputs and high-frequency in-situ measurements.


# Cat4KIT pipeline roadmap 
 
```mermaid 
%%{init: { 'theme':'forest'}}%%
gantt 
dateFormat YYYY-MM-DD 
title Cat4KIT development roadmap
section Cat4KIT dev
TDS2STAC V1(100%):done,tds2stac1,2022-10-01,2022-11-01
INTAKE2STAC V1(100%):done,intake2stac,2022-11-01,2022-11-15
STA2STAC V1(100%):done,sta2stac,2022-11-16,2022-11-30
DS2STAC-UI V1(100%):done,ds2stac-ui1,2022-12-01,2022-12-10
TDS2STAC V2(100.0%):done, 1964, 2023-06-12, 2023-07-05
Cat4KIT-UI V1(100.0%):done, 1965, 2023-07-06, 2023-08-20
Thumbnails(100.0%):done, 1973, 2023-09-20, 2023-11-30
DS2STAC V2(100.0%):done, 1970, 2023-08-21, 2023-11-30
Pipeline(93.3%):active, 2107, 2023-07-01, 2024-01-01
API Authentification(0.0%):active, 1975, 2023-12-10, 2024-01-10
Cat4KIT-UI V2(72.0%):active, 1972, 2023-12-07, 2024-01-15
Int. and Ext. harvester(100.0%):done, 1974, 2024-01-15, 2024-01-20
section Cat4KIT-docker
+pgSTAC(100%):done,pgstacdocekr,2022-12-11,2022-12-25
+STAC-FastAPI(100%):done,stacapdocker,2022-12-11,2022-12-25
+STAC-Browser(100%):done,stacbrowserdocker,2022-12-11,2022-12-25
+DS2STAC(100%):done,ds2stacdocker,2022-12-11,2022-12-25
Dockerizing(100.0%):done, 1985, 2023-07-05, 2024-01-01
section Research
Testify datasets(100.0%):done, 1979, 2023-08-21, 2023-11-15
Comparison research(0.0%):active, 1978, 2024-01-15, 2024-01-25
section Docs and Publications
DS2STAC Docs(100%):done,tds2stacdoc,2022-10-01,2022-11-30
Datahub(100%):done,datahub,2023-01-19,2023-01-20
E-Sceince-Tage23(100%):done,esciencetage23,2023-03-01,2023-03-03
EGU(100%):done,egu,2023-04-23,2023-04-28
DSS8(100%):done,dss8,2023-06-08,2023-06-09
DS2STAC doc(100.0%):done, 1981, 2023-11-01, 2023-11-30
Cat4KIT doc(14.3%):active, 1982, 2024-01-17, 2024-02-01
Cat4KIT article(0.0%):active, 1983, 2024-01-17, 2024-02-17
```

# Cat4KIT schematic diagram (Will be updated) 
[![Cat4KIT](https://codebase.helmholtz.cloud/cat4kit/gitlab-profile/-/raw/main/Cat4KIT_schematic.png)](https://www.mindmeister.com/map/2809466926?t=36rTXj1fJQ)